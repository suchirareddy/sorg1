<!-------------------------------------------------------------------------------------------
-                                                                                            
-         Name :      SandboxInfoMain
-       Author :      Pratyush (Infosys Limited)                                              
-  Description :  Developed for Sandbox Manager AppExchange product                           
-                 This is the main page of the application
-                                                                                             
-         © 2016 Infosys Limited, Bangalore, India. All rights reserved.                      
-         No part of this code should be used without prior written consent from the author
-------------------------------------------------------------------------------------------->
<apex:page controller="infy_sm.SandboxInfoMain" action="{!init}" applyHtmlTag="false" applyBodyTag="false" showHeader="false" sidebar="false" standardStylesheets="false" docType="html-5.0" id="thePage">
    <apex:includeScript value="{!$Resource.infy_sm__SecureFilters}"/>

    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <head>
            <title>Sandbox Manager by Infosys Limited</title>
            <style>
                .tableheader{
                color: red;
                }
            </style>
            <apex:stylesheet value="{!URLFOR($Resource.infy_sm__SLDS0102, 'assets/styles/salesforce-lightning-design-system-vf.css')}"/>
            <apex:includeScript value="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"/>
            <apex:includeScript value="https://code.jquery.com/jquery-2.2.0.min.js"/>
            <apex:includeScript value="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.25.1/js/jquery.tablesorter.min.js"/>
            <apex:includeScript value="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.25.1/js/jquery.tablesorter.widgets.min.js"/>
            <apex:includeScript value="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.25.1/js/extras/jquery.tablesorter.pager.min.js"/>
            <apex:stylesheet value="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.25.1/css/theme.metro-dark.min.css"/>
            <script>
            $(document).ready(function(){
                $("#dataTable").tablesorter({
                    theme : 'metro-dark',
                    widgets: ["zebra", "filter","resizable"],
                    sortList : [[2,0]],
                    cssHeader : "tableheader",
                    widgetOptions : {
                        filter_columnFilters : true,
                        filter_hideFilters : false,
                        filter_saveFilters : false,
                        resizable_addLastColumn : true,
                        resizable_widths : [ '10%', '10%', '10%', '30%', '15%', '15%' ]
                    }
                });
                toggleTrendBox();
            });

            var barChartLabel=eval('[{!JSENCODE(strbarChartLabels)}]');
            var barChartData = {
                labels : barChartLabel,
                datasets : [
                    {
                        label: "Total Licenses",
                        fillColor : "rgba(248, 137, 98, 0.5)",
                        strokeColor : "rgba(248, 137, 98, 0.8)",
                        highlightFill: "rgba(248, 137, 98, 0.75)",
                        highlightStroke: "rgba(248, 137, 98, 1)",
                        data : {!barChartData2}
                    },
                    {
                        label: "Used Licenses",
                        fillColor : "rgba(12, 142, 255,0.5)",
                        strokeColor : "rgba(12, 142, 255,0.8)",
                        highlightFill: "rgba(12, 142, 255,0.75)",
                        highlightStroke: "rgba(12, 142, 255,1)",
                        data : {!barChartData}
                    }
                ]
            }
            var strlineLabel=eval('{!JSENCODE(strlineLabels)}');  
            var lineData = {
                labels: strlineLabel,
                datasets: [
                    {
                        label: "Full Sandboxes",
                        fillColor: "rgba(248,137,98,0.2)",
                        strokeColor: "rgba(248,137,98,1)",
                        pointColor: "rgba(248,137,98,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(248,137,98,1)",
                        data: {!fullCount}
                    },
                    {
                        label: "Partial Sandboxes",
                        fillColor: "rgba(127,141,225,0.2)",
                        strokeColor: "rgba(127,141,225,1)",
                        pointColor: "rgba(127,141,225,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(127,141,225,1)",
                        data: {!partialCount}
                    },
                    {
                        label: "Developer Pro Sandboxes",
                        fillColor: "rgba(75,192,118,0.2)",
                        strokeColor: "rgba(75,192,118,1)",
                        pointColor: "rgba(75,192,118,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(75,192,118,1)",
                        data: {!devProCount}
                    },
                    {
                        label: "Developer Sandboxes",
                        fillColor: "rgba(239,126,173,0.2)",
                        strokeColor: "rgba(239,126,173,1)",
                        pointColor: "rgba(239,126,173,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(239,126,173,1)",
                        data: {!devCount}
                    }
                ]
            }
            window.onload = function(){
                var ctx2 = document.getElementById("chart2").getContext("2d");
                window.myBar = new Chart(ctx2).Bar(barChartData, {
                    responsive : true, 
                    scaleFontColor: "#EEF1F6",
                    showTooltips: true, 
                    multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
                    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li style=\"display:inline; padding-right: 5px;\"><span style=\"background-color:<%=datasets[i].strokeColor%>\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                });
                document.getElementById('chart2-legend').innerHTML = myBar.generateLegend();
                
                var ctx = document.getElementById("chart1").getContext("2d");

                var pieChartDatas=eval('{!JSENCODE(pieChartData)}');
                

                window.myPie = new Chart(ctx).Pie(pieChartDatas, {
                    responsive : true, 
                    scaleFontColor: "#EEF1F6", 
                    segmentShowStroke : false,
                    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li style=\"display:inline; padding-right: 5px;\"><span style=\"background-color:<%=segments[i].fillColor%>;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
                });
                document.getElementById('chart1-legend').innerHTML = myPie.generateLegend();
                
                var ctx3 = document.getElementById("chart3").getContext("2d");
                var chart3Datas=eval('{!JSENCODE(chart3Data)}');
                window.myPie2 = new Chart(ctx3).Pie(chart3Datas, {
                    responsive : true, 
                    scaleFontColor: "#EEF1F6", 
                    segmentShowStroke : false,
                    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li style=\"display:inline; padding-right: 5px;\"><span style=\"background-color:<%=segments[i].fillColor%>;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
                });
                document.getElementById('chart3-legend').innerHTML = myPie2.generateLegend();
                
                var ctx4 = document.getElementById("chart4").getContext("2d");
                window.myLine = new Chart(ctx4).Line(lineData, {
                    responsive : true
                });
            }
            
            function updateAdminEmail(recId){
                var emailId = document.getElementById(recId).value;
                updateEmail(recId,emailId);
            }
            
            function toggleTrendBox(){
                if($('#trendBox').css('display')=='block')
                    $('#trendBox').css('display','none');
                else{
                    $('#trendBox').css('display','block');
                    $('#chart4').css('width','80%');
                    $('#chart4').css('height',($( window ).height() * 0.5) + 'px');
                    var ctx4 = document.getElementById("chart4").getContext("2d");
                    window.myLine = new Chart(ctx4).Line(lineData, {
                        responsive : true, 
                        scaleFontColor: "#EEF1F6",
                        showTooltips: true, 
                        multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
                        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li style=\"display:inline; padding-right: 5px;\"><span style=\"background-color:<%=datasets[i].strokeColor%>\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                    });
                    document.getElementById('chart4-legend').innerHTML = myLine.generateLegend();
                }
            }
            </script>
            
        </head>
        <body>
            <apex:messages />
            <apex:outputLink rendered="{!(isError)}" value="/home/home.jsp" >Return Home</apex:outputLink>
            <apex:outputPanel styleClass="slds" layout="block" rendered="{!NOT(isError)}">
                
                <div class="slds-page-header" role="banner">
                    <div class="slds-grid">
                        <div class="slds-col">
                            <div class="slds-media">
                                <div class="slds-media__figure">
                                    <img src="{!URLFOR($Resource.SMIcon)}"/>
                                    <!--<span class="slds-icon__container slds-icon-custom-91 slds-p-around--small">
                                        <svg aria-hidden="true" class="slds-icon--large icon__svg slds-icon-custom-91">
                                            <use xlink:href="{!URLFOR($Resource.SLDS0102, '/assets/icons/action-sprite/svg/symbols.svg#new_custom91')}"></use>
                                        </svg>
                                    </span>-->
                                </div>
                                <div class="slds-media__body">                                    
                                    <h1 class="slds-text-heading--large">Sandbox Manager</h1>
                                    <p class="slds-text-heading--label">Infosys Limited</p>
                                </div>
                            </div>
                        </div>
                        <div class="slds-col">
                            <a class="slds-button slds-button--icon slds-float--right" href="/home/home.jsp">Home&nbsp;
                                <svg aria-hidden="true" class="slds-button__icon">
                                    <use xlink:href="{!URLFOR($Resource.SLDS0102, '/assets/icons/utility-sprite/svg/symbols.svg#home')}"></use>
                                </svg>
                                <span class="slds-assistive-text">Settings</span>
                            </a>
                        </div>
                    </div>
                </div>
                
                <div class="slds-grid slds-m-top--large">
                    <div class="slds-col">
                        <h3 class="slds-text-heading--label slds-m-around--medium">Sandboxes: Utilized vs Remaining</h3>
                        <canvas class="slds-box slds-theme--inverse" id="chart1">
                        </canvas>
                        <div id="chart1-legend" class="chart-legend"></div>
                    </div>
                    <div class="slds-col slds-m-left--medium">
                        <h3 class="slds-text-heading--label slds-m-around--medium">Sandboxes in Use By Type</h3>
                        <canvas class="slds-box slds-theme--inverse" id="chart2">
                        </canvas>
                        <div id="chart2-legend" class="chart-legend"></div>
                    </div>
                    <div class="slds-col slds-m-left--medium">
                        <h3 class="slds-text-heading--label slds-m-around--medium">Sandboxes Health Status</h3>
                        <canvas class="slds-box slds-theme--inverse" id="chart3">
                        </canvas>
                        <div id="chart3-legend" class="chart-legend"></div>
                    </div>
                </div>
                <span class="slds-type-focus" onclick="toggleTrendBox();">View Sandbox Trend >></span>
                <div class="slds-grid slds-m-top--large">
                    <div class="slds-col">
                        <apex:form id="dataForm">
                            <table class="tablesorter" id="dataTable" style="table-layout: fixed;">
                                <thead>
                                    <tr class="slds-text-heading--label slds-theme--inverse">
                                        <th>
                                            <span>Sandbox Name</span>
                                        </th>
                                        
                                        <th>
                                            <span>License Type</span>
                                        </th>
                                        
                                        <th>
                                            <span>Last Refreshed</span>
                                        </th>
                                        
                                        <th>
                                            <span>Description</span>
                                        </th>
                                        
                                        <th class="sorter-false filter-false">
                                            <span>Health Status</span>
                                        </th>
                                        
                                        <th class="sorter-false filter-false">
                                            <span>Admin Email</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <apex:repeat value="{!sandboxes_c}" var="rec">
                                        <tr class="slds-hint-parent">
                                            <td data-label="sandbox-name" role="row"><a href="/{!rec.Id}" class="slds-truncate">{!rec.Name}</a></td>
                                            <td data-label="license-type"><span class="slds-truncate">{!rec.License_Type__c}</span></td>
                                            <td data-label="last-refreshed"><span class="slds-truncate"><apex:outputField value="{!rec.infy_sm__Last_Refreshed__c}"/></span></td>
                                            <td data-label="description" style="overflow: hidden;text-overflow: ellipsis;white-space: nowrap;min-width: 10px;"><span class="slds-truncate">{!rec.infy_sm__Description__c}</span></td>
                                            <td data-label="health-status">
                                                <apex:outputField value="{!rec.infy_sm__Health_Status_Icon__c}"/>
                                                <apex:outputText value="Overdue by {!rec.infy_sm__Refresh_Overdue_By__c} days" rendered="{!IF(rec.infy_sm__Health_Status__c='Needs Refresh',true,false)}"/>
                                                <apex:outputText value="Due in {!rec.infy_sm__Refresh_Overdue_By__c} days" rendered="{!IF(rec.infy_sm__Health_Status__c='Due this Week',true,false)}"/>
                                            </td>
                                            <td data-label="admin-email">
                                                <input type="text" id="{!rec.Id}" value="{!rec.AdminEmail__c}"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <apex:outputLink styleClass="button" onclick="updateAdminEmail('{!rec.Id}')" >Save</apex:outputLink>
                                            </td>
                                        </tr> 
                                    </apex:repeat>
                                </tbody>
                            </table>
                        </apex:form>
                    </div>
                </div>
                
                
                <div id="trendBox">
                    <div aria-hidden="true" role="dialog" class="slds-modal slds-modal--large slds-fade-in-open">
                        <div class="slds-modal__container">
                            <div class="slds-modal__header">
                                <h2 class="slds-text-heading--medium">Sandbox Trends</h2>
                                <button class="slds-button slds-button--icon slds-modal__close" onclick="toggleTrendBox();">
                                    <svg aria-hidden="true" class="slds-button__icon slds-button__icon--large">
                                        <use xlink:href="{!URLFOR($Resource.SLDS0102,'/assets/icons/action-sprite/svg/symbols.svg#close')}"></use>
                                    </svg>
                                    <span class="slds-assistive-text">Close</span>
                                </button>
                            </div>
                            <div class="slds-modal__content">
                                <apex:outputPanel rendered="{!render4}" layout="block">
                                    <canvas id="chart4" class="slds-box slds-theme--inverse" width="80%" height="150px"/>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!NOT(render4)}" layout="block" styleClass="slds-box slds-theme--warning">
                                    <div class="slds-text-heading--label">No Trends established yet. Please check back later.</div>
                                </apex:outputPanel>
                            </div>
                            <div class="slds-modal__footer" id="chart4-legend">
                                
                            </div>
                        </div>
                    </div>
                    <div class="slds-backdrop slds-backdrop--open"></div>
                </div>
            </apex:outputPanel>
        </body>
    </html>
    <apex:form >
        <apex:actionFunction action="{!updateAdminEmail}" name="updateEmail" reRender="dataTable">
            <apex:param assignTo="{!recId}" value="" name="recId"/>
            <apex:param assignTo="{!emailId}" value="" name="emailId"/>
        </apex:actionFunction>
    </apex:form>
</apex:page>