public with sharing class CandidatePaging {
    
    public apexpages.StandardSetController canList{get;set;}
    
    public candidatePaging(apexPages.StandardController controller){
        canList=new apexPages.standardSetController(Database.getQueryLocator([select name,First_name__c,Last_name__c from candidate__c limit 10000]));
        canlist.setPagesize(2);
    }
    
    public list<candidate__c> getCandidates(){
        return (list<candidate__c>) canList.getRecords();
    }
    
}